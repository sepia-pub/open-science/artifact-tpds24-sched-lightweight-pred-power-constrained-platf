#!/usr/bin/env python3
import argparse
import random
import sys
import pandas as pd

HOST_TEMPLATE='''
    <host id="{hostname}" coordinates="0 0 0" speed="1.0Gf" core="{nb_cores}" pstate="0" >
        <prop id="wattage_per_state" value="{power_idle}:{power_epsilon}:{power_max}" />
        <prop id="wattage_off" value="10" />
        <prop id="original_M100_node" value="{m100_node}" />
    </host>'''

PLATFORM_TEMPLATE='''<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd">
<platform version="4.1">
<zone id="AS0" routing="Full">
    <host id="master_host" coordinates="0 0 0" speed="100Mf">
        <prop id="wattage_per_state" value="0:0:0" />
        <prop id="wattage_off" value="0" />
    </host>
    {hosts}
</zone>
</platform>
'''

def generate_platform_content(powermodel_df, nb_cores_per_host, seed):
    assert nb_cores_per_host > 0
    assert 'node' in powermodel_df
    assert 'min_power' in powermodel_df
    assert 'max_power' in powermodel_df

    host_ids = [f'{x:03d}' for x in range(len(powermodel_df))]
    if seed is not None:
        assert seed >= 0
        random.seed(seed)
        random.shuffle(host_ids)
    powermodel_df['host_id'] = host_ids
    powermodel_df['node_str'] = powermodel_df['node'].apply('{:03d}'.format)

    hosts = ''
    for index, row in powermodel_df.iterrows():
        host_content = HOST_TEMPLATE.format(
            hostname=row['host_id'],
            nb_cores=nb_cores_per_host,
            power_idle=row['min_power'],
            power_epsilon=row['min_power'],
            power_max=row['max_power'],
            m100_node=row['node_str'],
        )
        hosts = hosts + host_content

    return PLATFORM_TEMPLATE.format(hosts=hosts)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_machine_power_consumption_model", help='path to the CSV file that contains the power consumption model of each node')
    parser.add_argument("nb_cores_per_host", type=int, help='the number of cores to set in each SimGrid host')
    parser.add_argument("--seed", type=int, help='if set, shuffle host order by the given (integer) seed')
    parser.add_argument("-o", "--output_file", help="if set, write platform file into this file instead of stdout")
    args = parser.parse_args()

    powermodel_df = pd.read_csv(args.input_machine_power_consumption_model)
    platform_content = generate_platform_content(powermodel_df, args.nb_cores_per_host, args.seed)

    f = sys.stdout
    if args.output_file is not None:
        f = open(args.output_file, 'w')
    f.write(platform_content)
    if args.output_file is not None:
        f.close()
