import pandas as pd
import subprocess
import sys
import argparse
import os
import functools
from multiprocessing import Pool

"""
Run Workflow
"""
def run_workflow(jobfile, outdir): 
    # Load the dataframe with "user_id" column
    df = pd.read_csv(jobfile)     

    # Iterate over each user_id and run predict_jobs_power_svr.py
    for user_id in df['user_id'].drop_duplicates():
        outfile = outdir+'/filter123_user_'+str(user_id)+'_total_power_mean_pred.csv.gz'
        # Skip user if prediction was already performed 
        if os.path.isfile(outfile) == True:
            continue
        command = ['predict-jobs-power-allmethods-max', "-j", jobfile , '-o', outfile, '-u', str(user_id)]
        subprocess.run(command)

"""
Read Command line interface
"""
def read_cli():
    # Make parser object
    p = argparse.ArgumentParser(description='Run the sophisticated power prediction method, but one call for each user')    
    
    p.add_argument("--jobfile", "-j", type=str, required=True,
                   help="Job table file with power aggregation metrics (output from marconi_jobs_extract_power_metrics.py)")
    p.add_argument("--outdir", "-o", type=str, required=True,
                   help="Directory of the output files")    

    return(p.parse_args())

def main():
    if sys.version_info<(3,5,0):
        sys.stderr.write("You need python 3.5 or later to run this script\n")
        sys.exit(1)

    args = read_cli()
    run_workflow(args.jobfile, args.outdir)

if __name__ == '__main__':
    main()
