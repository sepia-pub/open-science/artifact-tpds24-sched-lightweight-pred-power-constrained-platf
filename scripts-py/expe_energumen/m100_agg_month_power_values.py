#!/usr/bin/env python3
import argparse
import os
import pandas as pd

def read_aggregate_month(input_filename):
    input_df = pd.read_parquet(input_filename).astype({'value': 'int64', 'node': 'int64'})

    occurrences = input_df.groupby(['value', 'node']).count().reset_index().rename({
        'value': 'power',
        'timestamp': 'nbocc',
    }, axis='columns')
    return occurrences

def one_month():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("output_file")
    args = parser.parse_args()

    output_df = read_aggregate_month(args.input_file)
    output_df.to_csv(args.output_file, index=False, header=['power', 'node', 'nbocc'])

def several_months():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_prefix", help='filepath prefix to where input parquet files are')
    parser.add_argument("output_prefix", help="filepath prefix used to write result CSV files")
    parser.add_argument("month", nargs='+', help="the month to aggregate. example value: '22-07'")
    args = parser.parse_args()

    full_total_df = None

    for month in args.month:
        month_prefix = f'{args.input_prefix}{month}'
        total_file = f'{month_prefix}_power_total.parquet'

        total_df = read_aggregate_month(total_file)
        if full_total_df is None:
            full_total_df = total_df
        else:
            full_total_df = pd.concat([full_total_df, total_df])
        del total_df

    total_agg = full_total_df.groupby(['power', 'node']).sum().reset_index()
    total_agg.to_csv(f'{args.output_prefix}power_total.csv', index=False)
