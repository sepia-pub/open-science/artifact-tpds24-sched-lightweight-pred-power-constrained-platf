#!/usr/bin/env python3
import argparse
import datetime
import json
import random
import subprocess
import sys

EPOCH_M100 = datetime.datetime(year=2022, month=1, day=1)

def main():
    datetime_parser = lambda f: datetime.datetime.strptime(f, '%Y-%m-%d %H:%M:%S')
    parser = argparse.ArgumentParser()
    parser.add_argument("workload_params_filepath", type=str, help='filepath to the workload parameters')
    parser.add_argument("input_jobs", help='path to the CSV file that contains the jobs information')
    parser.add_argument("input_power_timeseries_prefix", help="filepath prefix to the location of the parquet files that contain node power consumption time series")
    parser.add_argument("-o", "--output_dir", required=True, help="filepath where all the workload-related files should be generated")
    args = parser.parse_args()

    with open(args.workload_params_filepath) as f:
        params = json.load(f)

    for index, param in enumerate(params):
        process_args = [
            'm100-generate-batsim-workload',
            args.input_jobs,
            args.input_power_timeseries_prefix,
            '-o', args.output_dir,
            '-p', 'delay',
            '--begin', param['start_dt'],
            '--end', param['end_dt'],
        ]
        print(f"{datetime.datetime.now()} starting generation of workload {index+1}/{len(params)} (start_dt={param['start_dt']})")
        subprocess.run(process_args, check=True)

    print(f"{datetime.datetime.now()} all workloads have been generated")
