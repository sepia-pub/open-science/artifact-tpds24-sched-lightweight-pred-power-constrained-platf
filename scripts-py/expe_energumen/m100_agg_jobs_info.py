#!/usr/bin/env python3
import argparse
import csv
import os
import pandas as pd

def read_month(input_filename):
    input_df = pd.read_parquet(input_filename)
    return input_df[['job_id', 'nodes', 'num_nodes', 'submit_time', 'start_time', 'end_time', 'time_limit_str']]

def several_months():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_prefix", help='filepath prefix to where input parquet files are')
    parser.add_argument("output_file", help="where to write the result CSV file")
    parser.add_argument("month", nargs='+', help="the month to aggregate. example value: '22-07'")
    args = parser.parse_args()

    full_jobs_df = None
    for month in args.month:
        month_prefix = f'{args.input_prefix}{month}'
        jobs_file = f'{month_prefix}_jobs.parquet'

        jobs_df = read_month(jobs_file)
        if full_jobs_df is None:
            full_jobs_df = jobs_df
        else:
            full_jobs_df = pd.concat([full_jobs_df, jobs_df])
        del jobs_df

    full_jobs_df.sort_values(by=['job_id']).to_csv(args.output_file, index=False, quoting=csv.QUOTE_NONNUMERIC)
