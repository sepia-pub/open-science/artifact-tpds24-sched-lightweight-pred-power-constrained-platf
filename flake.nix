{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=23.11";
    flake-utils.url = "github:numtide/flake-utils";
    nur-kapack = {
      url = "github:oar-team/nur-kapack/master";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    intervalset-flake = {
      url = "git+https://framagit.org/batsim/intervalset";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batprotocol-flake = {
      url = "git+https://framagit.org/batsim/batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.flake-utils.follows = "flake-utils";
    };
    batsim-flake = {
      url = "git+https://framagit.org/batsim/batsim?ref=batprotocol";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.batprotocol.follows = "batprotocol-flake";
      inputs.intervalset.follows = "intervalset-flake";
      inputs.flake-utils.follows = "flake-utils";
    };
    easy-powercap-flake = {
      url = "git+https://gitlab.irit.fr/sepia-pub/batsim/easy-powercap.git?ref=TPDS";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nur-kapack.follows = "nur-kapack";
      inputs.batprotocol-flake.follows = "batprotocol-flake";
      inputs.intervalset-flake.follows = "intervalset-flake";
      inputs.flake-utils.follows = "flake-utils";
    };
    typst-flake = {
      url = "github:typst/typst?rev=21c78abd6eecd0f6b3208405c7513be3bbd8991c";
    };
  };

  outputs = { self, nixpkgs, nur-kapack, intervalset-flake, flake-utils, batprotocol-flake, batsim-flake, easy-powercap-flake, typst-flake }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        pkgs = import nixpkgs { inherit system; };
        py = pkgs.python3;
        pyPkgs = pkgs.python3Packages;
        kapack = nur-kapack.packages.${system};
        batprotopkgs = batprotocol-flake.packages.${system};
        intervalsetpkgs = intervalset-flake.packages.${system};
        batpkgs = batsim-flake.packages-release.${system};
        easy-powercap-pkgs = easy-powercap-flake.packages.${system};
        typstpkgs = typst-flake.packages.${system};
      in rec {
        packages = {
          batsim = batpkgs.batsim;
          python-scripts = pyPkgs.buildPythonPackage rec {
            name = "expe-en-py-scripts";
            format = "pyproject";
            src = pkgs.lib.sourceByRegex ./scripts-py [
              "^pyproject\.toml$"
              "^expe_energumen"
              "^expe_energumen/.*\.py"
            ];
            nativeBuildInputs = with pyPkgs; [
              setuptools
              setuptools-scm
            ];
            propagatedBuildInputs = with pyPkgs; [
              packages.fastparquet-2402
              pyPkgs.sortedcontainers
              pyPkgs.scipy
              pyPkgs.scikit-learn
            ];
          };
          fastparquet-2402 = pyPkgs.fastparquet.overrideAttrs(final: prev: rec {
            version = "2024.2.0";
            src = pkgs.fetchFromGitHub {
              owner = "dask";
              repo = prev.pname;
              rev = version;
              hash = "sha256-e0gnC/HMYdrYdEwy6qNOD1J52xgN2x81oCG03YNsYjg=";
            };
          });
          easypower-sched-lib = easy-powercap-pkgs.easypower;
        };
        devShells = rec {
          download-m100-months = pkgs.mkShell {
            buildInputs = [
              packages.python-scripts
              pkgs.coreutils # md5sum
              pkgs.curl # curl
              pkgs.gnutar # tar
            ];
          };
          merge-m100-power-predictions = pkgs.mkShell {
            buildInputs = [
              packages.python-scripts
              pkgs.gnutar # tar
              pkgs.gzip # gunzip
            ];
          };
          py-scripts = pkgs.mkShell {
            buildInputs = [
              packages.python-scripts
            ];
          };
          simulation = pkgs.mkShell {
            buildInputs = [
              packages.batsim
              packages.easypower-sched-lib
              packages.python-scripts
            ];
            BATSIM_ROOT_PATH="${batsim-flake}";
            EDC_LIBRARY_PATH="${packages.easypower-sched-lib}/lib";
          };
          r-notebook = pkgs.mkShell {
            buildInputs = [
              pkgs.pandoc
              pkgs.R
              pkgs.rPackages.rmarkdown
              pkgs.rPackages.rmdformats
              pkgs.rPackages.knitr
              pkgs.rPackages.svglite
              pkgs.rPackages.tidyverse
              pkgs.rPackages.viridis
            ];
          };
          r-py-notebook = pkgs.mkShell {
            buildInputs = r-notebook.buildInputs ++ [
              pkgs.rPackages.reticulate
              pyPkgs.pandas
              pyPkgs.seaborn
              pyPkgs.scikit-learn
            ];
          };
          typst-shell = pkgs.mkShell {
            buildInputs = [
              typstpkgs.typst-dev
            ];
          };
        };
      }
    );
}
