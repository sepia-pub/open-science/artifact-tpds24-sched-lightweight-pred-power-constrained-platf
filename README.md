Artifact code of article "Scheduling with lightweight predictions in power-constrained HPC platforms" submitted to TPDS in 2024.

Artifact data and the guide to reproduce our work is available on Zenodo: https://zenodo.org/doi/10.5281/zenodo.13961003

### Looking for the Euro-Par 24 base paper instead?
This work is an extension from the article "Light-weight prediction for improving energy consumption in HPC platforms" published at Euro-Par 2024.
- Europar24 artifact repo: https://gitlab.irit.fr/sepia-pub/open-science/artifact-europar24-lightweight-power-pred-sched
- Europar24 data & guide on Zenodo: https://zenodo.org/doi/10.5281/zenodo.11173631
